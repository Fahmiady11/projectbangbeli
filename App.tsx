/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React, { type PropsWithChildren, useState } from 'react';
import tw from 'twrnc';

import {
  SafeAreaView,
  // ScrollView,
  // StatusBar,
  Text,
  // useColorScheme,
  // View,
  Image,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';

import {
  // Colors,
  // DebugInstructions,
  // Header,
  // LearnMoreLinks,
  ReloadInstructions,

} from 'react-native/Libraries/NewAppScreen';

// const Section: React.FC<
//   PropsWithChildren<{
//     title: string;
//   }>
// > = ({children, title}) => {
//   const isDarkMode = useColorScheme() === 'dark';
//   return (
//     <View style={styles.sectionContainer}>
//       <Text
//         style={[
//           styles.sectionTitle,
//           {
//             color: isDarkMode ? Colors.white : Colors.black,
//           },
//         ]}>
//         {title}
//       </Text>
//       <Text
//         style={[
//           styles.sectionDescription,
//           {
//             color: isDarkMode ? Colors.light : Colors.dark,
//           },
//         ]}>
//         {children}
//       </Text>
//     </View>
//   );
// };

const App = () => {
  const [state, setState] = useState(0);
  return (
    <SafeAreaView style={tw`h-full w-full bg-white flex items-center justify-center`}>
      <Image
        style={tw`h-64 w-64`}
        source={require('./src/images/home.png')}
      />
      <Text style={tw`text-yellow-500 font-bold text-xl`}>{state}</Text>
      <TouchableOpacity
        style={tw`bg-yellow-500 p-3 mt-3 rounded-md`}
        onPress={() => { setState(state + 1) }}
      >
        <Text style={tw`text-white font-semibold`}>Press Here</Text>
      </TouchableOpacity>
    </SafeAreaView>
  );
};

export default App;
